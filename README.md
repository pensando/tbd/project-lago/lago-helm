<!-- GETTING STARTED -->
## Getting Started

This repository contains helm charts for Pensando Lago Project.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* kubectl
  ```sh
  brew install kubectl
  ```
* helm3
  ```sh
  brew install helm
  ```

### Installation - Otto

1. Download VM ova file.
2. Import image into virtualbox using a bridged adapter.
3. Connect to the VM and use tbdadmin/Pensando0$ to login, or use ssh
   ```sh
   ssh tbdadmin$@XXX.XXX.XXX.XXX
3. Upgrade Lago for your specific configuration
   ```sh
   helm upgrade lago pensando/lago -f values-files/values-otto.yaml -n pensando
   ```
### Installation - All-In-One

1. Get a docker repository key from Pensando team
2. Create namespace & change context to that namespace so everything is done in that namespace
   ```sh
   kubectl create namespace pensando
   kubectl config set-context --current --namespace=pensando
   ```
3. Add image pull secret to the cluster
    ```sh
    kubectl create secret docker-registry pensando-image-creds --docker-server=registry.gitlab.com --docker-username=gitlab+deploy-token-349500 --docker-password=sufyMzq4GAaebsLjcBYU --docker-email=deploy@pensando.io
    ```
    

4. Add Strimzi chart repository
   ```sh
   helm repo add strimzi https://strimzi.io/charts/
   ```
5. Add Pensando chart repository
   ```sh
   helm repo add pensando https://pensando.gitlab.io/tbd/project-lago/lago-helm
   ```
6. Install Kafka Operator
   ```sh
   helm install kafka-operator strimzi/strimzi-kafka-operator -n pensando --create-namespace --version 0.33.2 --set extraEnvs[0].name=KUBERNETES_SERVICE_DNS_DOMAIN,extraEnvs[0].value=cluster.local
   ```
7. Install Lago
   ```sh
   helm install lago pensando/lago -f values-files/values-S.yaml -n pensando --create-namespace
   ```

### Installation - Separate Components

1. Get a docker repository key from Pensando team.
2. Create namespace & change context to that namespace so everything is done in that namespace
   ```sh
   kubectl create namespace pensando
   kubectl config set-context --current --namespace=pensando
   ```
3. Add image pull secret to the cluster
    ```sh
    kubectl create secret docker-registry pensando-image-creds --docker-server=registry.gitlab.com --docker-username=$username --docker-password=$password --docker-email=deploy@pensando.io

4. Add Strimzi chart repository
   ```sh
   helm repo add strimzi https://strimzi.io/charts/
   ```
5. Add Pensando chart repository
   ```sh
   helm repo add pensando https://pensando.gitlab.io/tbd/lago-helm
   ```
6. Install Kafka Operator
   ```sh
   helm install kafka-operator strimzi/strimzi-kafka-operator -n pensando --create-namespace --version 0.27.1 --set extraEnvs[0].name=KUBERNETES_SERVICE_DNS_DOMAIN,extraEnvs[0].value=cluster.local
   ```
7. Install Kafka Cluster Definition
   ```sh
   helm install kafka kafka-cluster -n pensando --create-namespace
   ```
8. Install PSM Collector Service
   ```sh
   helm install collector psm-collect -n pensando --create-namespace
   ```
9. Install IpFix Ingest Service
  ```sh
  helm install ipfix ipfix-ingest -n pensando --create-namespace
  ```
10. Install FW Log Ingest Service
  ```sh
  helm install syslog syslog-ingest -n pensando --create-namespace
  ```
### Monitoring

1. Add Prometheus Helm Repo
   ```sh
   helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
   ```
3. Install Prometheus Operator
   ```sh
   helm install monitoring prometheus-community/kube-prometheus-stack --set prometheus.prometheusSpec.serviceMonitorSelectorNilUsesHelmValues=false
   ```
3. Enable Lago Service Monitoring
   ```sh
   helm upgrade lago pensando-lago -n pensando --create-namespace -f /values-files/values-monitoring-example.yaml
   ```
4. Access Grafana
    ```sh
    kubectl port-forward service/monitoring-grafana 3000:80
    ```
    Username: admin Password:prom-operator.

5. The lago metrics are under pensando_* label

### Sending Traffic To The Cluster

1. The IPFix service is listening on port UDP/30739.
2. The DSC Log service is listening on port UDP/TCP/30005
3. The Events Log service is listening on port UDP/TCP/30006
4. The DSS Log service is listening on port UDP/TCP/30007
5. Kafka Bootstrap service is listening on port TCP/32092


### Exposing services on lower ports
1. Add NGINX Ingress Repository
   ```sh
   helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
   ```

2. Install NGINX Ingress
   ```sh
   helm install ingress-nginx ingress-nginx/ingress-nginx -n pensando -f /values-files/values-nginx-hostport-ingress.yaml
   ```
   
   
   
